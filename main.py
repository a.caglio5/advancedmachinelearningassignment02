import pandas as pd
import os

from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split
from assignment02.functions import preprocess_data, neural_network, risultati, convert_in_binary, \
    plot_history, layers_inspection, l1_regularization, l2_regularization, weight_control, model_dropout, \
    education_correction, marriage_correction, bill_correction

# DATA DIRECTORY
current_directory = os.getcwd()
train_folder = current_directory + "/train/"
test_folder = current_directory + "/test/"

# IMPORT DATA
X_train = pd.read_csv(train_folder + "X_train.csv", sep=",", header=0, index_col=0, encoding="ISO-8859-2")
y_train = pd.read_csv(train_folder + "y_train.csv", sep=",", header=0, index_col=0, encoding="ISO-8859-2")
X_test = pd.read_csv(test_folder + "X_test.csv", sep=",", header=0, index_col=0, encoding="ISO-8859-2")

# PRE-PROCESS DATA
# Correct instances with abnormal education value
X_train['EDUCATION'] = X_train['EDUCATION'].apply(lambda education: education_correction(education))
X_test['EDUCATION'] = X_test['EDUCATION'].apply(lambda education: education_correction(education))

# Correct instances with abnormal marriage value
X_train['MARRIAGE'] = X_train['MARRIAGE'].apply(lambda marriage: marriage_correction(marriage))
X_test['MARRIAGE'] = X_test['MARRIAGE'].apply(lambda marriage: marriage_correction(marriage))

# Correct instances with negative bill value
X_train['BILL_AMT1'] = X_train['BILL_AMT1'].apply(lambda bill: bill_correction(bill))
X_test['BILL_AMT1'] = X_test['BILL_AMT1'].apply(lambda bill: bill_correction(bill))
X_train['BILL_AMT2'] = X_train['BILL_AMT2'].apply(lambda bill: bill_correction(bill))
X_test['BILL_AMT2'] = X_test['BILL_AMT2'].apply(lambda bill: bill_correction(bill))
X_train['BILL_AMT3'] = X_train['BILL_AMT3'].apply(lambda bill: bill_correction(bill))
X_test['BILL_AMT3'] = X_test['BILL_AMT3'].apply(lambda bill: bill_correction(bill))
X_train['BILL_AMT4'] = X_train['BILL_AMT4'].apply(lambda bill: bill_correction(bill))
X_test['BILL_AMT4'] = X_test['BILL_AMT4'].apply(lambda bill: bill_correction(bill))
X_train['BILL_AMT5'] = X_train['BILL_AMT5'].apply(lambda bill: bill_correction(bill))
X_test['BILL_AMT5'] = X_test['BILL_AMT5'].apply(lambda bill: bill_correction(bill))
X_train['BILL_AMT6'] = X_train['BILL_AMT6'].apply(lambda bill: bill_correction(bill))
X_test['BILL_AMT6'] = X_test['BILL_AMT6'].apply(lambda bill: bill_correction(bill))

X_train, scaler = preprocess_data(X_train)
X_test, _ = preprocess_data(X_test, scaler)

# KERAS

X_train, X_validation, y_train, y_validation = train_test_split(X_train, y_train, test_size=0.1, random_state=0)

act_func = 'relu'

print('\nTraining with -->{0}<-- activation function\n'.format(act_func))
dims = y_train.shape[1]
model = neural_network(activation=act_func, X=X_train, n_outputs=dims)
model.summary()

number_of_epochs = 100

network_history = model.fit(X_train, y_train,
                            batch_size=16,
                            epochs=number_of_epochs,
                            verbose=1,
                            validation_data=(X_validation, y_validation))

x_plot = list(range(1, number_of_epochs + 1))
accuracy = plot_history(x_plot, network_history)

risultati(act_func, accuracy)

# CONFUSION MATRIX & ACCURACY OF TRAINING SET (model without regularization)
y_train_predicted = model.predict(X_train)

df_y_train_predicted = pd.DataFrame(y_train_predicted)
df_y_train_predicted[0] = df_y_train_predicted[0].apply(lambda label: convert_in_binary(label))

conf_mat = confusion_matrix(y_train, df_y_train_predicted)
print("\nConfusion Matrix of training set: (model without regularization)\n", conf_mat)

accuracy = accuracy_score(y_train, df_y_train_predicted)
print("\nAccuracy of training set: (model without regularization)", accuracy, "\n")

# CONFUSION MATRIX & ACCURACY OF VALIDATION SET (model without regularization)
y_validation_predicted = model.predict(X_validation)

df_y_validation_predicted = pd.DataFrame(y_validation_predicted)
df_y_validation_predicted[0] = df_y_validation_predicted[0].apply(lambda label: convert_in_binary(label))

conf_mat = confusion_matrix(y_validation, df_y_validation_predicted)
print("\nConfusion Matrix of validation set: (model without regularization)\n", conf_mat)

accuracy = accuracy_score(y_validation, df_y_validation_predicted)
print("\nAccuracy of validation set: (model without regularization)", accuracy, "\n")

# LAYERS INSPECTION
layers_inspection(model)

# L1 REGULARIZATION
model_l1 = l1_regularization(activation=act_func, X=X_train, n_outputs=dims)

network_history_l1 = model_l1.fit(X_train, y_train,
                                  batch_size=16,
                                  epochs=number_of_epochs,
                                  verbose=1,
                                  validation_data=(X_validation, y_validation))

x_plot = list(range(1, number_of_epochs + 1))
accuracy_l1 = plot_history(x_plot, network_history_l1)

risultati(act_func, accuracy_l1)

# CONFUSION MATRIX & ACCURACY OF TRAINING SET (model with L1 regularization)
y_train_predicted = model_l1.predict(X_train)

df_y_train_predicted = pd.DataFrame(y_train_predicted)
df_y_train_predicted[0] = df_y_train_predicted[0].apply(lambda label: convert_in_binary(label))

conf_mat = confusion_matrix(y_train, df_y_train_predicted)
print("\nConfusion Matrix of training set: (model with L1 regularization)\n", conf_mat)

accuracy = accuracy_score(y_train, df_y_train_predicted)
print("\nAccuracy of training set: (model with L1 regularization)", accuracy, "\n")

# CONFUSION MATRIX & ACCURACY OF VALIDATION SET (model with L1 regularization)
y_validation_predicted = model_l1.predict(X_validation)

df_y_validation_predicted = pd.DataFrame(y_validation_predicted)
df_y_validation_predicted[0] = df_y_validation_predicted[0].apply(lambda label: convert_in_binary(label))

conf_mat = confusion_matrix(y_validation, df_y_validation_predicted)
print("\nConfusion Matrix of validation set: (model with L1 regularization)\n", conf_mat)

accuracy = accuracy_score(y_validation, df_y_validation_predicted)
print("\nAccuracy of validation set: (model with L1 regularization)", accuracy, "\n")

# L2 REGULARIZATION
model_l2 = l2_regularization(activation=act_func, X=X_train, n_outputs=dims)

network_history_l2 = model_l2.fit(X_train, y_train,
                                  batch_size=16,
                                  epochs=number_of_epochs,
                                  verbose=1,
                                  validation_data=(X_validation, y_validation))

x_plot = list(range(1, number_of_epochs + 1))
accuracy_l2 = plot_history(x_plot, network_history_l2)

risultati(act_func, accuracy_l2)

# CONFUSION MATRIX & ACCURACY OF TRAINING SET (model with L2 regularization)
y_train_predicted = model_l2.predict(X_train)

df_y_train_predicted = pd.DataFrame(y_train_predicted)
df_y_train_predicted[0] = df_y_train_predicted[0].apply(lambda label: convert_in_binary(label))

conf_mat = confusion_matrix(y_train, df_y_train_predicted)
print("\nConfusion Matrix of training set: (model with L2 regularization)\n", conf_mat)

accuracy = accuracy_score(y_train, df_y_train_predicted)
print("\nAccuracy of training set: (model with L2 regularization)", accuracy, "\n")

# CONFUSION MATRIX & ACCURACY OF VALIDATION SET (model with L2 regularization)
y_validation_predicted = model_l2.predict(X_validation)

df_y_validation_predicted = pd.DataFrame(y_validation_predicted)
df_y_validation_predicted[0] = df_y_validation_predicted[0].apply(lambda label: convert_in_binary(label))

conf_mat = confusion_matrix(y_validation, df_y_validation_predicted)
print("\nConfusion Matrix of validation set: (model with L2 regularization)\n", conf_mat)

accuracy = accuracy_score(y_validation, df_y_validation_predicted)
print("\nAccuracy of validation set: (model with L2 regularization)", accuracy, "\n")

# WEIGHT CONTROL
weight_control(model)
weight_control(model_l1)
weight_control(model_l2)

print('Sum of the values of the weights without regularization:', sum(abs(model.weights[2][0])).numpy())
print('Sum of the values of the weights with regularization l1:', sum(abs(model_l1.weights[2][0])).numpy())
print('Sum of the values of the weights with regularization l2:', sum(abs(model_l2.weights[2][0])).numpy())

# DROPOUT
model_d = model_dropout(activation=act_func, X=X_train, n_outputs=dims)

network_history_d = model_d.fit(X_train, y_train,
                                batch_size=16,
                                epochs=number_of_epochs,
                                verbose=1,
                                validation_data=(X_validation, y_validation))

x_plot = list(range(1, number_of_epochs + 1))
accuracy_d = plot_history(x_plot, network_history_d)

risultati(act_func, accuracy_d)

# CONFUSION MATRIX & ACCURACY OF TRAINING SET (model with dropout)
y_train_predicted = model_d.predict(X_train)

df_y_train_predicted = pd.DataFrame(y_train_predicted)
df_y_train_predicted[0] = df_y_train_predicted[0].apply(lambda label: convert_in_binary(label))

conf_mat = confusion_matrix(y_train, df_y_train_predicted)
print("\nConfusion Matrix of training set: (model with dropout)\n", conf_mat)

accuracy = accuracy_score(y_train, df_y_train_predicted)
print("\nAccuracy of training set: (model with dropout)", accuracy, "\n")

# CONFUSION MATRIX & ACCURACY OF VALIDATION SET (model with dropout)
y_validation_predicted = model_d.predict(X_validation)

df_y_validation_predicted = pd.DataFrame(y_validation_predicted)
df_y_validation_predicted[0] = df_y_validation_predicted[0].apply(lambda label: convert_in_binary(label))

conf_mat = confusion_matrix(y_validation, df_y_validation_predicted)
print("\nConfusion Matrix of validation set: (model with dropout)\n", conf_mat)

accuracy = accuracy_score(y_validation, df_y_validation_predicted)
print("\nAccuracy of validation set: (model with dropout)", accuracy, "\n")

# PREDICT Y_TEST (model with dropout)
y_test = model_d.predict(X_test)

df_y_test = pd.DataFrame(y_test)
df_y_test[0] = df_y_test[0].apply(lambda label: convert_in_binary(label))
df_y_test.to_csv('y_test.csv')
