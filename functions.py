import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from keras import *
from keras.optimizers import SGD
from keras.layers.core import Dense, Dropout


def education_correction(education):
    if education == 0 or education == 5 or education == 6:
        education = 4
    return education


def marriage_correction(marriage):
    if marriage == 0:
        marriage = 3
    return marriage


def bill_correction(bill):
    if bill < 0:
        bill = bill * (-1)
    return bill


def preprocess_data(x, scaler=None):
    """Preprocess input data by standardise features
    by removing the mean and scaling to unit variance"""
    if not scaler:
        scaler = StandardScaler()
        scaler.fit(x)
    x = scaler.transform(x)
    return x, scaler


def neural_network(X, activation, n_outputs):
    dims = X.shape[1]
    initializer = tf.keras.initializers.GlorotUniform(seed=1234)

    model = Sequential()
    model.add(Dense(64, input_shape=(dims,), activation=activation, kernel_initializer=initializer))
    model.add(Dense(64, activation=activation, kernel_initializer=initializer))
    model.add(Dense(n_outputs, activation='sigmoid', kernel_initializer=initializer))
    model.compile(loss='binary_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy'])
    return model


def plot_history(x_plot, network_history):
    plt.figure()
    plt.title('Model Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')

    plt.plot(x_plot, network_history.history['loss'])
    plt.plot(x_plot, network_history.history['val_loss'])
    plt.legend(['Training', 'Validation'])

    plt.figure()
    plt.title('Model Accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')

    plt.plot(x_plot, network_history.history['accuracy'])
    plt.plot(x_plot, network_history.history['val_accuracy'])
    plt.legend(['Training', 'Validation'], loc='lower right')

    accuracy = network_history.history['accuracy'][-1]

    plt.show()

    return accuracy


def risultati(activation_function, accuracy):
    print('Accuracy of the activation function {} is {}'.format(activation_function, round(accuracy, 5)))


def convert_in_binary(label):
    if label <= 0.5:
        label = 0
    else:
        label = 1
    return label


def layers_inspection(model):
    print('Model Input Tensors: ', model.input, end='\n\n')
    print('Layers - Network Configuration:', end='\n\n')
    for layer in model.layers:
        print(layer.name, layer.trainable)
        print('Layer Configuration:')
        print(layer.get_config(), end='\n{}\n'.format('----' * 10))
    print('Model Output Tensors: ', model.output)

    for layer in range(len(model.get_weights())):
        print('Layer name:', model.weights[layer].name)
        print('Layer weights shape:', model.weights[layer].shape)
        print('Weight:', model.weights[layer], end='\n\n')
    print('Model Output Tensors: ', model.output)


def l1_regularization(X, activation, n_outputs):
    dims = X.shape[1]
    initializer = tf.keras.initializers.GlorotUniform(seed=1234)

    model = Sequential()
    # model.add(Dense(64, input_shape=(dims,), activation=activation, kernel_initializer=initializer,
    #                kernel_regularizer=regularizers.l1(0.01)))
    model.add(Dense(64, input_shape=(dims,), activation=activation, kernel_initializer=initializer,
                    bias_regularizer=regularizers.l1(0.01)))
    # model.add(Dense(64, activation=activation, kernel_initializer=initializer,
    #                kernel_regularizer=regularizers.l1(0.01)))
    model.add(Dense(64, activation=activation, kernel_initializer=initializer,
                    bias_regularizer=regularizers.l1(0.01)))
    model.add(Dense(n_outputs, activation='sigmoid', kernel_initializer=initializer))
    model.compile(loss='binary_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy'])
    return model


def l2_regularization(X, activation, n_outputs):
    dims = X.shape[1]
    initializer = tf.keras.initializers.GlorotUniform(seed=1234)

    model = Sequential()
    # model.add(Dense(64, input_shape=(dims,), activation=activation, kernel_initializer=initializer,
    #                 kernel_regularizer=regularizers.l2(0.01)))
    model.add(Dense(64, input_shape=(dims,), activation=activation, kernel_initializer=initializer,
                    bias_regularizer=regularizers.l2(0.01)))
    # model.add(Dense(64, activation=activation, kernel_initializer=initializer,
    #                 kernel_regularizer=regularizers.l2(0.01)))
    model.add(Dense(64, activation=activation, kernel_initializer=initializer,
                    bias_regularizer=regularizers.l2(0.01)))
    model.add(Dense(n_outputs, activation='sigmoid', kernel_initializer=initializer))
    # model.add(Dense(n_outputs, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy'])
    return model


def weight_control(model):
    print('Layers name:', model.weights[2].name)
    print('Layers kernel shape:', model.weights[2].shape)
    print('Kernel:', model.weights[2][0], end='\n\n')
    print('Layers name:', model.weights[3].name)
    print('Layers kernel shape:', model.weights[3].shape)
    print('Kernel:', model.weights[3])


def model_dropout(X, activation, n_outputs):
    dims = X.shape[1]
    initializer = tf.keras.initializers.GlorotUniform(seed=1234)

    model = Sequential()
    model.add(Dense(64, input_shape=(dims,), activation=activation, kernel_initializer=initializer))
    model.add(Dense(64, activation=activation, kernel_initializer=initializer))
    model.add(Dropout(0.2))
    model.add(Dense(n_outputs, activation='sigmoid', kernel_initializer=initializer))
    model.compile(loss='binary_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy'])
    return model
